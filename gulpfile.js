const
	dest_folder = require("path").basename(__dirname),
	src_folder = "./#src",

	path = {
		dest: {
			pug: dest_folder + "/",
			json: src_folder + "/template/json/",
			scss: dest_folder + "/css/",
			js: dest_folder + "/js/",
			images: dest_folder + "/images/",
			fonts: dest_folder + "/fonts/",
			fonts_convert: {
				ttf: src_folder + "/template/fonts/",
				otf: src_folder + "/fonts/"
			},
			fonts_style: src_folder + "/fonts/",
			iconFonts: src_folder + "/template/fonts/",
			svgSprite: dest_folder + "/images/",
			pngSprite: dest_folder + "/images/"
		},
		src: {
			pug: src_folder + "/pages/*.pug",
			json: src_folder + "/blocks/**/*.json",
			scss: src_folder + "/template/scss/#main.scss",
			js: [src_folder + "/plugins/jquery/jquery.js",
				src_folder + "/plugins/**/*.js",
				src_folder + "/blocks/**/*.js"],
			images: src_folder + "/blocks/**/*.{jpg,png,gif,svg,webp,ico}",
			fonts: src_folder + "/template/fonts/**/*.{ttf,oft,woff,woff2,svg}",
			fonts_convert: {
				ttf: src_folder + "/fonts/**/*.ttf",
				otf: src_folder + "/fonts/**/*.otf"
			},
			fonts_style: src_folder + "/template/scss/fonts.scss",
			iconFonts: src_folder + "/fonts/icons/*.svg",
			svgSprite: src_folder + "/sprite/svg/*.svg",
			pngSprite: src_folder + "/sprite/png/*.png"
		},
		watch: {
			pug: [src_folder + "/pages/*.pug",
			src_folder + "/blocks/**/*.pug",
			src_folder + "/template/pug/*.pug",
			src_folder + "/template/json/*.json"],
			json: src_folder + "/blocks/**/*.json",
			scss: [src_folder + "/blocks/**/*.scss",
			src_folder + "/template/scss/*.scss"],
			js: [src_folder + "/blocks/**/*.js",
			src_folder + "/plugins/**/*.js"],
			images: src_folder + "/blocks/**/*.{jpg,png,gif,svg,webp,ico}",
			fonts: src_folder + "/template/fonts/**/*.{ttf,otf,woff,woff2,svg}",
			fonts_convert: src_folder + "/fonts/**/*.{otf,ttf}",
			iconFonts: src_folder + "/fonts/icons/*.svg",
			svgSprite: src_folder + "/sprite/svg/*.svg",
			pngSprite: src_folder + "/sprite/png/*.png"
		},
		clean: dest_folder + "/",
		json: {
			name: "main.json"
		},
		js: {
			name: "main.js"
		},
		iconFonts: {
			runTimestamp: Math.round(Date.now() / 1000),
			targetPath: "../scss/_iconFont.scss",
			fontPath: "../fonts/",
			pathTemplate: src_folder + "/template/scss/_templateIconFont.scss",
			fontName: "iconFont"
		},
		ftp: {
			src: dest_folder + "/**/*",
			host: "fh7917vc.beget.tech",
			user: "fh7917vc_view",
			pass: "IKy0q%o4",
			folder: dest_folder
		}
	},

	fs = require("fs"),
	gulp = require("gulp"),
	g_clean = require("del"),
	g_browsersync = require("browser-sync"),
	g_pug = require("gulp-pug"),
	g_jsonConcat = require("gulp-json-concat"),
	g_scss = require("gulp-sass"),
	g_sourcemaps = require("gulp-sourcemaps"),
	g_group_media = require("gulp-group-css-media-queries"),
	g_autoprefixer = require("gulp-autoprefixer"),
	g_concat = require("gulp-concat"),
	g_rename = require("gulp-rename"),
	g_ttf2woff = require("gulp-ttf2woff"),
	g_ttf2woff2 = require("gulp-ttf2woff2"),
	g_fonter = require("gulp-fonter"),
	g_iconFont = require("gulp-iconfont"),
	g_iconFont_css = require("gulp-iconfont-css"),
	g_svgsprite = require("gulp-svg-sprite"),
	g_pngsprite = require("gulp.spritesmith"),
	g_ftp = require("gulp-ftp"),
	g_util = require("gulp-util"),

	merge = require("merge-stream"),
	notify = require("gulp-notify");


// Локальный сервер
gulp.task("browsersync", () => {
	g_browsersync.init({
		server: {
			baseDir: dest_folder,
			index: "_map.html"
		},
		notify: false
	})
});

// Отслеживание изменения файлов
gulp.task("watch", () => {
	gulp.watch(path.watch.pug, gulp.series("pug"));
	gulp.watch(path.watch.json, gulp.series("json"));
	gulp.watch(path.watch.scss, gulp.series("scss"));
	gulp.watch(path.watch.js, gulp.series("js"));
	gulp.watch(path.watch.images, gulp.series("images"));
	gulp.watch(path.watch.fonts, gulp.series("fonts"));
	// gulp.watch(path.watch.ttf2woff, gulp.series("fontsConvert"));
	gulp.watch(path.watch.iconFonts, gulp.series("iconFonts"));
	gulp.watch(path.watch.svgSprite, gulp.series("svgSprite"));
	gulp.watch(path.watch.pngSprite, gulp.series("pngSprite"));
});

// Удаление папки результата сборки
gulp.task("clean", () => {
	return g_clean([
		dest_folder
	]);
});

// Обработка html(pug)
gulp.task("pug", () => {
	return gulp.src(path.src.pug)
		.pipe(g_pug({
			locals: {
				main: JSON.parse(fs.readFileSync(path.dest.json + path.json.name, "utf-8"))
			},
			pretty: true
		}))
		.on("error", notify.onError(function (error) {
			return {
				title: "Pug",
				message: error.message
			};
		}))
		.pipe(gulp.dest(path.dest.pug))
		.pipe(g_browsersync.stream());
});

// Обработка фалов .json
gulp.task("json", () => {
	return gulp.src(path.src.json)
		.pipe(g_jsonConcat(path.json.name, function (data) {
			return new Buffer(JSON.stringify(data));
		}))
		.pipe(gulp.dest(path.dest.json));
});

// Обработка css(scss)
gulp.task("scss", () => {
	return gulp.src(path.src.scss)
		.pipe(g_sourcemaps.init())
		.pipe(g_scss())
		.on("error", notify.onError(function (error) {
			return {
				title: "Scss",
				message: error.message
			}
		}))
		.pipe(g_group_media())
		.pipe(g_autoprefixer({
			overrideBrowserslist: ['last 5 version'],
			cascade: true
		}))
		.pipe(g_sourcemaps.write())
		.pipe(g_rename({
			basename: "main"
		}))
		.pipe(gulp.dest(path.dest.scss))
		.pipe(g_browsersync.stream());
});

// Обработка js
gulp.task("js", () => {
	return gulp.src(path.src.js)
		.pipe(g_concat(path.js.name))
		.pipe(gulp.dest(path.dest.js))
		.pipe(g_browsersync.stream());
});

// Обработка images
gulp.task("images", () => {
	return gulp.src(path.src.images)
		.pipe(g_rename({
			dirname: ''
		}))
		.pipe(gulp.dest(path.dest.images))
		.pipe(g_browsersync.stream());
});

// Создание svg спрайта
gulp.task("svgSprite", () => {
	return gulp.src(path.src.svgSprite)
		.pipe(g_svgsprite({
			mode: {
				stack: {
					sprite: "../svgSprite.svg",
					example: false
				}
			}
		}))
		.pipe(gulp.dest(path.dest.svgSprite))
		.pipe(g_browsersync.stream());
});

// Создание png спрайта
gulp.task("pngSprite", () => {
	var spriteData = gulp.src(path.src.pngSprite)
		.pipe(g_pngsprite({
			imgName: 'pngSprite.png',
			imgPath: "../images/pngSprite.png",
			cssName: '_pngSprite.scss',
			padding: 5,
			cssVarMap: function (sprite) {
				sprite.name = 'png-sprite--' + sprite.name;
			},
			cssTemplate: src_folder + "/template/scss/_pngSpriteTemplate.scss"
		}));
	var imgStream = spriteData.img
		.pipe(gulp.dest(path.dest.pngSprite));
	var cssStream = spriteData.css
		.pipe(gulp.dest(src_folder + "/template/scss/"));

	return merge(imgStream, cssStream);
});

// Обработка шрифтов
gulp.task("fonts", () => {
	gulp.src(src_folder + "/fonts/**/*.{woff,woff2}")
		.pipe(gulp.dest(src_folder + "/template/fonts/"));
	return gulp.src(path.src.fonts)
		.pipe(gulp.dest(path.dest.fonts))
		.pipe(g_browsersync.stream());
});

// Конвертация шрифтов
gulp.task("otf2ttf", () => {
	return gulp.src(path.src.fonts_convert.otf)
		.pipe(g_fonter({
			formats: ["ttf"]
		}))
		.pipe(gulp.dest(path.dest.fonts_convert.otf));
});

// Конвертация шрифтов
gulp.task("ttf2woff", () => {
	gulp.src(path.src.fonts_convert.ttf)
		.pipe(g_ttf2woff())
		.pipe(gulp.dest(path.dest.fonts_convert.ttf));
	return gulp.src(path.src.fonts_convert.ttf)
		.pipe(g_ttf2woff2())
		.pipe(gulp.dest(path.dest.fonts_convert.ttf));
});

// Авто подключение шрифтов
gulp.task("fontsStyle", () => {
	let file_content = fs.readFileSync(path.src.fonts_style);
	if (file_content == '') {
		fs.writeFile(path.src.fonts_style, '', cb);
		return fs.readdir(path.dest.fonts_style, function (err, items) {
			if (items) {
				let c_fontname;
				for (var i = 0; i < items.length; i++) {
					let fontname = items[i].split('.');
					fontname = fontname[0];
					if (c_fontname != fontname) {
						fs.appendFile(path.src.fonts_style, '@include font("' + fontname + '", "' + fontname + '", "400", "normal");\r\n', cb);
					}
					c_fontname = fontname;
				}
			}
		});
	}
});

// Создание иконочного шрифта
gulp.task("iconFonts", () => {
	return gulp.src(path.src.iconFonts)
		.pipe(g_iconFont_css({
			path: path.iconFonts.pathTemplate,
			fontName: path.iconFonts.fontName,
			targetPath: path.iconFonts.targetPath,
			fontPath: path.iconFonts.fontPath
		}))
		.pipe(g_iconFont({
			fontName: path.iconFonts.fontName,
			prependUnicode: true,
			formats: ["woff", "woff2"],
			timestamp: path.iconFonts.runTimestamp
		}))
		.pipe(gulp.dest(path.dest.iconFonts))
		.pipe(g_browsersync.stream());
});

function cb() { }

// Загрузка файлов на хост для просмотра
gulp.task("ftp", () => {
	return gulp.src(path.ftp.src)
		.pipe(g_ftp({
			host: path.ftp.host,
			user: path.ftp.user,
			pass: path.ftp.pass,
			remotePath: path.ftp.folder
		}))
		.pipe(g_util.noop());
});

// Вывод адреса загруженых файлов
gulp.task("siteAddress", () => {
	console.log('http://aelor-code.ru/view/' + dest_folder + '/_map.html');
	return false;
});

gulp.task("fontsConvert", gulp.series("otf2ttf", "ttf2woff", "fontsStyle"));

gulp.task("default", gulp.series(
	"clean", "json", "iconFonts", "pngSprite",
	gulp.parallel(
		"pug", "scss", "js", "images", "fonts", "svgSprite"
	),
	gulp.parallel(
		"watch",
		"browsersync"
	)
));

gulp.task("load", gulp.series("ftp", "siteAddress"));