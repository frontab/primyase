$('.block__message-area').on('input change', function() {

    var val = $(this).text(),
        input = $(this).siblings('input');

    if ( val.length ) {
        $(this).closest('.block__message')
            .addClass('edit');
    } else {
        $(this).closest('.block__message')
            .removeClass('edit');
    }

    input.val(val);
});