$('.cards-nums a').on('click', function(e) {
	e.preventDefault();

	$(this).addClass('active')
		.closest('li')
		.siblings()
		.find('.active')
		.removeClass('active');
});