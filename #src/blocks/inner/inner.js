if ($('.aside-box--fixed').length !=0 ) {
  var testAside = true;
} else {
  var testAside = false;
}

$(document).on('resize', function() {
  if($(window).width() >= 1200 && testAside) {
      asideScrolled('.aside-box--fixed','.inner__main');
  }
  });

if (testAside) {
  asideScrolled('.aside-box--fixed','.inner__main');
}