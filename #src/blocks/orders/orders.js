$(document).on('click', '.order__btn, .order-item__toggle', function(e) {
	e.preventDefault();

	$(this).closest('.order')
		.toggleClass('active');
});


$(document).on('click', '.order-item__trigger', function(e) {
	e.preventDefault();

	$(this).closest('.order-item__items')
		.toggleClass('active');
});


ordersListMive();
$(window).on('resize', function() {
	ordersListMive();
});

function ordersListMive() {
  	var point = 991, //брейкпоинт для перехода
  		sectionMove = $('.order-item__items'); // Перемещаемый блок

  	if ( sectionMove.length ) {

		sectionMove.each(function() {
			var asideBox = $(this).closest('.order-item').find('.order-item__top'), // куда переместить
				list = $(this).closest('.order-item').find('.order-item__info'); // откуда перемещалось

			if ( $(window).width() <= point && !$(this).hasClass('moved') ) {
				asideBox.after($(this));
				$(this).addClass('moved');
			}
			if ( $(window).width() > point && $(this).hasClass('moved') ) {
				list.after($(this));
				$(this).removeClass('moved');
			}
		});
  	}
}