$(document).on('click', '.onTop', function(e) {
	e.preventDefault();

	$('html,body').animate({scrollTop: 0}, 500);
});

onTopCheck();
$(window).on('scroll', function() {
	onTopCheck();
});

function onTopCheck() {
	if ($('.onTop').length) {
		var wTop = $(window).scrollTop(),
			wBottom = wTop + $(window).height(),
			footerTop = $('.footer').offset().top;
	
		if ( wTop >= 400 ) {
			$('.onTop').addClass('active');
		} else {
			$('.onTop').removeClass('active');
		}
	
		var mod = 0;
		if ( wBottom > footerTop ) {
			mod = wBottom - footerTop;
		} else {
			mod = 0;
		}
	
		$('.onTop').css({
			"transform":"translateY(-" + mod + "px)"
		});
	}
}