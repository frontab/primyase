sliderMobileOther();
$(window).on('resize', function() {
	sliderMobileOther();
});
function sliderMobileOther() {
	if ( $(window).width() < 1200 ) {
		if ($('.slider-mobile-other').hasClass('slick-slider')) {} else {
			$('.slider-mobile-other').slick({
				infinite: false,
				slidesToShow: 4,
				slidesToScroll: 1,
				arrows: false,
				dots: true,
				swipeToSlide: true,
				autoSlidesToShow: true,
				responsive: [
					{
						breakpoint: 991,
						settings: {
							slidesToShow: 3
						}
					},
					{
						breakpoint: 767,
						settings: {
							slidesToShow: 1,
							fade: true,
							adaptiveHeight: true
						}
					}
				]
			});
		}
	} else {
		if ($('.slider-mobile-other').hasClass('slick-slider')) {
			$('.slider-mobile-other').slick('unslick');
		}
	}
}

sliderMobileAlt();
$(window).on('resize', function() {
	sliderMobileAlt();
});
function sliderMobileAlt() {
	if ( $(window).width() < 992 ) {
		if ($('.slider-mobile-alt').hasClass('slick-slider')) {} else {
			$('.slider-mobile-alt').slick({
				infinite: false,
				slidesToShow: 3,
				slidesToScroll: 1,
				arrows: false,
				dots: true,
				swipeToSlide: true,
				autoSlidesToShow: true,
				responsive: [
					{
						breakpoint: 565,
						settings: {
							slidesToShow: 2
						}
					}
				]
			});
		}
	} else {
		if ($('.slider-mobile-alt').hasClass('slick-slider')) {
			$('.slider-mobile-alt').slick('unslick');
		}
	}
}

sliderMobile();
$(window).on('resize', function() {
	sliderMobile();
});
function sliderMobile() {
	if ( $(window).width() < 992 ) {
		if ($('.slider-mobile').hasClass('slick-slider')) {} else {
			$('.slider-mobile').slick({
				infinite: false,
				slidesToShow: 1,
				slidesToScroll: 1,
				arrows: false,
				dots: true
			});
		}
	} else {
		if ($('.slider-mobile').hasClass('slick-slider')) {
			$('.slider-mobile').slick('unslick');
		}
	}
}

sliderMobileDoubleAlt();
$(window).on('resize', function() {
	sliderMobileDoubleAlt();
});
function sliderMobileDoubleAlt() {
	if ( $(window).width() < 992 ) {
		if ($('.slider-mobile-double-alt').hasClass('slick-slider')) {} else {
			$('.slider-mobile-double-alt').slick({
				infinite: false,
				slidesToShow: 3,
				slidesToScroll: 1,
				arrows: false,
				dots: true,
				swipeToSlide: true,
				autoSlidesToShow: true,
				responsive: [
				 {
					breakpoint: 565,
					settings: {
						slidesToShow: 2,
						slidesToScroll: 1
					}
				 }
				]
			});
		}
	} else {
		if ($('.slider-mobile-double-alt').hasClass('slick-slider')) {
			$('.slider-mobile-double-alt').slick('unslick');
		}
	}
}

sliderMobileDouble();
$(window).on('resize', function() {
	sliderMobileDouble();
});
function sliderMobileDouble() {
	if ( $(window).width() < 1200 ) {
		if ($('.slider-mobile-double').hasClass('slick-slider')) {} else {
			$('.slider-mobile-double').slick({
				infinite: false,
				slidesToShow: 3,
				slidesToScroll: 1,
				arrows: false,
				dots: true,
				swipeToSlide: true,
				autoSlidesToShow: true,
				responsive: [
				 {
					breakpoint: 565,
					settings: {
						slidesToShow: 2,
						slidesToScroll: 1
					}
				 }
				]
			});
		}
	} else {
		if ($('.slider-mobile-double').hasClass('slick-slider')) {
			$('.slider-mobile-double').slick('unslick');
		}
	}
}

$('.slider--default').slick({
	infinite: false,
	slidesToShow: 4,
	slidesToScroll: 1,
	arrows: true,
	swipeToSlide: true,
	autoSlidesToShow: true,
	prevArrow: '<button class="slick-prev"><i class="icon icon-angle-left"></i></button>',
	nextArrow: '<button class="slick-next"><i class="icon icon-angle-right"></i></button>',
	responsive: [
		{
			breakpoint: 991,
			settings: {
				arrows: false,
				dots: true,
				slidesToShow: 3
			}
		},
		{
			breakpoint: 565,
			settings: {
				arrows: false,
				dots: true,
				slidesToShow: 2
			}
		}
	]
});

$('.slider--default-card').slick({
	infinite: false,
	slidesToShow: 4,
	slidesToScroll: 1,
	arrows: true,
	swipeToSlide: true,
	autoSlidesToShow: true,
	prevArrow: '<button class="slick-prev"><i class="icon icon-angle-left"></i></button>',
	nextArrow: '<button class="slick-next"><i class="icon icon-angle-right"></i></button>',
	responsive: [
		{
			breakpoint: 1200,
			settings: {
				slidesToShow: 3
			}
		},
		{
			breakpoint: 991,
			settings: {
				arrows: false,
				dots: true,
				slidesToShow: 3
			}
		},
		{
			breakpoint: 565,
			settings: {
				arrows: false,
				dots: true,
				slidesToShow: 2
			}
		}
	]
});

sliderSimpleDesktop();
$(window).on('resize', function() {
	sliderSimpleDesktop();
});
function sliderSimpleDesktop() {
	if ( $(window).width() >= 992 ) {
		if ($('.slider--simple').hasClass('slick-slider')) {} else {
			$('.slider--simple').slick({
				infinite: false,
				slidesToShow: 4,
				slidesToScroll: 1,
				arrows: true,
				swipeToSlide: true,
				autoSlidesToShow: true,
				prevArrow: '<button class="slick-prev"><i class="icon icon-angle-left"></i></button>',
				nextArrow: '<button class="slick-next"><i class="icon icon-angle-right"></i></button>',
				variableWidth: true
			});
		}
	} else {
		if ($('.slider--simple').hasClass('slick-slider')) {
			$('.slider--simple').slick('unslick');
		}
	}
}

$(document).on('mouseover', '.slider--default-card .slider__item', function() {
	$(this).closest('.slider--default-card')
		.addClass('vis');
});

$(document).on('mouseout', '.slider--default-card .slider__item', function() {
	$(this).closest('.slider--default-card')
		.removeClass('vis');
});


// $(document).ready(function(){
// 	var dots = $('.slider li');
// 	//вешаем обработчик на наши точки
// 	dots.click(function(){
// 		var $this = $(this);
// 		dots.removeClass('before after');
// 		//отображаем 2 предыдущие точки
// 		$this
// 			.prev().addClass('before')
// 			.prev().addClass('before');
// 		//отображаем 2 следующие точки
// 		$this
// 			.next().addClass('after')
// 			.next().addClass('after');

	
// 	  //если мы в самом начале - добавляем пару последующих точек
// 		if(!$this.prev().length) {
// 	  	$this.next().next().next()
// 		  	.addClass('after').next()
// 			  .addClass('after');
//   	}
// 		//на 2й позиции - добавляем одну точку
// 	  if(!$this.prev().prev().length) {
//   		$this.next().next().next()
// 	  	  .addClass('after');
//   	}
// 		//в самом конце - добавляем пару доп. предыдущих точек
// 		if(!$this.next().length) {
// 			$this.prev().prev().prev()
// 				.addClass('before').prev()
// 				.addClass('before');
// 		}
// 		//предпоследний элемента - добавляем одну пред. точку
// 		if(!$this.next().next().length) {
// 			$this.prev().prev().prev()
// 				.addClass('before');
// 		}	
// 	});
// 	dots.eq(0).click();//кликаем на первую точку
// });