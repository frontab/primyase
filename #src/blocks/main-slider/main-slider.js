$('.main-slider').slick({
	infinite: true,
	// autoplay: true,
	arrows: true,
	dots: true,
	prevArrow: '<button class="slick-prev"><i class="icon icon-long-arrow-left"></i></button>',
	nextArrow: '<button class="slick-next"><i class="icon icon-long-arrow-right"></i></button>',
	slidesToShow: 1,
	slidesToScroll: 1
});