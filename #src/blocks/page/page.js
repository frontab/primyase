$(document).on('click', '[data-link-scroll]', function(e) {
	e.preventDefault();

	var boxName = $(this).attr('data-link-scroll'),
		section = $(`[data-box-scroll="${boxName}"]`),
		sectionTop = section.offset().top,
		mod = 60;

	$('html,body').animate({scrollTop: sectionTop - mod}, 500);
});

$('.date-select').styler();


Tipped.create('.tooltip', {
	skin: 'purple'
});