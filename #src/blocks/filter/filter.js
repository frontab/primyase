$(document).on('click', '.filter-sel__label', function(e) {
	e.preventDefault();

	$(this).closest('.filter-sel')
		.toggleClass('opened');
});

$(document).on('click', '.filter-sel__dropdown li', function(e) {
	e.preventDefault();

	$(this).addClass('selected')
		.siblings('.selected')
		.removeClass('selected')
		.closest('.filter-sel')
		.removeClass('opened')
		.find('.filter-sel__val')
		.html($(this).html());
});

$(document).mouseup(function (e) {
    var container = $(".filter-sel");
    if (container.has(e.target).length === 0){
        container.removeClass('opened');
    }
});