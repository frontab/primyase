$(document).on('click', '.search-box__btn', function(e) {
	e.preventDefault();

	$(this).closest('.search-box')
		.toggleClass('active')
		.find('input')
		.focus();
});

$(document).mouseup(function (e) {
	var boxArea = $('.isFixed .search-box');
	if (boxArea.has(e.target).length === 0) {
		boxArea.removeClass('active');
	}
});


$(document).on('click', '.search-box__close', function(e) {
	e.preventDefault();

	$(this).closest('.search-box')
		.removeClass('active');
});

$(document).on('input change', '.search-box input', function() {
	var val = $(this).val();

	if ( val.length ) {
		$(this).closest('.search-box')
			.find('.search-box__rezult')
			.addClass('active');
	} else {
		$(this).closest('.search-box')
			.find('.search-box__rezult')
			.removeClass('active');
	}
	
});

$(document).mouseup(function (e) {
	var boxArea = $('.search-box__rezult');
	if (boxArea.has(e.target).length === 0) {
		boxArea.removeClass('active');
	}
});