$(document).on('click', '.select-custom__select', function(e) {
	e.preventDefault();

	$(this).closest('.select-custom')
		.toggleClass('active');
});
$(document).mouseup(function (e) {
	var boxArea = $('.select-custom');
	if (boxArea.has(e.target).length === 0) {
		boxArea.removeClass('active');
	}
});

$(document).on('click', '.select-custom__dropdown li', function(e) {
	e.preventDefault();

	var val = $(this).html();

	$(this).addClass('selected')
		.siblings('.selected')
		.removeClass('selected')
		.closest('.select-custom')
		.removeClass('active')
		.addClass('edit')
		.find('.select-custom__text')
		.html(val);
});