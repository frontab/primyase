$(document).on('click', '.section__trigger', function(e) {
	e.preventDefault();

	$(this).closest('.basket-section')
		.toggleClass('active');

	$('.basket-slider').slick('refresh');
});

$(document).on('click', '.js-basket-trigger', function(e) {
	e.preventDefault();

	$('.page').toggleClass('checkout-step');
	$('body,html').animate({scrollTop: 0}, 0);
});

$('.basket-slider').slick({
	infinite: false,
	slidesToShow: 6,
	slidesToScroll: 1,
	arrows: true,
	dots: false,
	swipeToSlide: true,
	autoSlidesToShow: true,
	prevArrow: '<button class="slick-prev"><i class="icon icon-angle-left"></i></button>',
	nextArrow: '<button class="slick-next"><i class="icon icon-angle-right"></i></button>',
	responsive: [
		{
			breakpoint: 767,
			settings: {
				slidesToShow: 2,
				arrows: false,
				dots: true,
				variableWidth: true
			}
		}
	]
});


$('.basket-slider__item-btn').on('click', function(e) {
	e.preventDefault();

	$(this).closest('.basket-slider__item')
		.addClass('added');
});