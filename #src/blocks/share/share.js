$(document).mouseup(function (e) {
    var container = $(".share-box");
    if (container.has(e.target).length === 0){
        container.removeClass('active');
    }
});

$(document).on('click', '.share-box__btn', function(e) {
    e.preventDefault();

    $(this).closest('.share-box')
        .toggleClass('active');
});