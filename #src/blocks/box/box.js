$(document).on('click', '.box-code button', function(e) {
	e.preventDefault();

	var val = $(this).siblings('input').val();

	if (val && val != ' ') {
		$(this).closest('.box-block--trigger')
			.removeClass('active')
			.addClass('edit')
			.find('.box-block__val')
			.addClass('edit')
			.text('- 500 ₽');
	}
});

$(document).on('click', '.box-block__trigger', function(e) {
	e.preventDefault();

	$(this).closest('.box-block--trigger')
		.toggleClass('active');
});