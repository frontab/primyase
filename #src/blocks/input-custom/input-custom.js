$(document).on('input change', '.input-custom__val', function() {
	var val = $(this).text(),
		box = $(this).closest('.input-custom'),
		input = box.find('.form__input');

	input.val(val);

	if (val) {
		box.addClass('edit');
	} else {
		box.removeClass('edit');
	}
});