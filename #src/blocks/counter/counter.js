$('.counter__btn').on('click', function(e) {
	e.preventDefault();

	var btnType = $(this).attr('data-type');

	if ( btnType == "del" ) {
		// удаление товара из корзины
		return false;
	}

	var counter = $(this).closest('.counter'),
		counterStep = counter.attr('data-step').replace(",","."),
		counterVal = counter.find('.counter__val-num'),
		counterValNum = counterVal.text().replace(",","."),
		newVal;

	if ( btnType == "dec" ) {
		newVal = (parseFloat(counterValNum) - parseFloat(counterStep));
	}

	if ( btnType == "inc" ) {
		newVal = (parseFloat(counterValNum) + parseFloat(counterStep));
	}

	if ( isFloarNum(newVal) ) {
		newVal = newVal.toFixed(1) + "";
		newVal = newVal.replace(".",",");
	}
	if (newVal < 1) {
		newVal = 1;
	}
	counterVal.text(newVal);

	counterCheck(counter);
});


if ( $('.counter') ) {
	counterInit();
}

function counterInit() {
	$('.counter').each(function() {
		counterCheck($(this));
	});
}

function counterCheck(counter) {
	var counterStep = counter.attr('data-step'),
		couterVal = counter.find('.counter__val-num').text();

	if ( counterStep == couterVal ) {
		counter.find('.counter__btn[data-type="del"]').addClass('active');
	} else {
		counter.find('.counter__btn[data-type="del"]').removeClass('active');
	}
}

function isFloarNum(num) {
	return !((num ^ 0) === num);
}