$('.part-box').on('mouseover click', function() {
	var tag = $(this).attr('data-tag');

	$('.part-img svg').find(`[data-box="${tag}"]`)
		.addClass('active');
});
$('.part-box').on('mouseout', function() {
	var tag = $(this).attr('data-tag');

	$('.part-img svg').find(`[data-box="${tag}"]`)
		.removeClass('active');
});

$('.part-img [data-box]').on('mouseover click', function() {
	var tag = $(this).attr('data-box');
	$(this).addClass('active')
		.siblings(`[data-box="${tag}"]`)
		.addClass('active');
	$(`.part-box[data-tag="${tag}"]`).addClass('hover');
});

$('.part-img [data-box]').on('mouseout', function() {
	var tag = $(this).attr('data-box');
	$(this).removeClass('active')
		.siblings(`[data-box="${tag}"]`)
		.removeClass('active');
	$(`.part-box[data-tag="${tag}"]`).removeClass('hover');
});