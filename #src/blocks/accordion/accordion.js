$(document).on('click', '.accordion-box__btn', function(e) {
	e.preventDefault();

	$(this).closest('.accordion-box')
		.toggleClass('active')
		.find('.slick-slider').slick('refresh');
});