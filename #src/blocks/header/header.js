headerFixed();
$(window).on('scroll resize', function() {
	headerFixed();
});
function headerFixed() {
	if ($('.header').length) {
		if ( $(window).width() >= 1200 ) {
			var marginMod = 220,
				breakpointTop = 300,
	
				wScroll = $(window).scrollTop(),
				wWindow = $(window).width(),
	
				content = $('.content'),
				boxFixed = $('.header');
	
			if ( wWindow >= 1200 && wWindow < 1340 ) {
				marginMod = 210;
			}
	
			if ( wScroll >= breakpointTop ) {
				content.css({"padding-top": marginMod});
				boxFixed.addClass('isFixed');
			} else {
				content.prop("style", "");
				boxFixed.removeClass('isFixed');
			}
	
			menuInit(); // обновление меню
		}
	}
	if ($('.header-mobile').length) {
		if ( $(window).width() < 1200 ) {
			var breakpointTop = 96,
				wScroll = $(window).scrollTop(),
				boxFixed = $('.header-mobile');
	
			if ( wScroll >= breakpointTop ) {
				boxFixed.addClass('isFixed');
			} else {
				boxFixed.removeClass('isFixed');
			}
		}
	}
}



$(document).on('click', '.header-mobile__user', function(e) {
	e.preventDefault();

	$(this).closest('.header-mobile__user')
		.toggleClass('active');
});
$(document).mouseup(function (e) {
	var boxArea = $('.header-mobile__user');
	if (boxArea.has(e.target).length === 0) {
		boxArea.removeClass('active');
	}
});

$(document).on('click', '.header-mobile__menu button', function(e) {
	e.preventDefault();

	$('body').toggleClass('mobile-menu-opened');
});