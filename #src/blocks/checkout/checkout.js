$(document).on('click', '.checkout-sel__value', function(e) {
	e.preventDefault();

	$('.checkout-sel.active').removeClass('active');

	$(this).closest('.checkout-sel')
		.toggleClass('active');
});

$(document).mouseup(function (e) {
	var boxArea = $('.checkout-sel');
	if (boxArea.has(e.target).length === 0) {
		boxArea.removeClass('active');
	}
});

$(document).on('click', '.checkout-sel .form__label', function() {
	if ( $(window).width() < 768 ) {
		var val = $(this).find('.form__label-name').html();

		$(this).closest('.checkout-sel')
			.removeClass('active')
			.find('.checkout-sel__value')
			.html(val);
	}
});

$(document).on('click', '.checkout-date', function(e) {
	e.preventDefault();

	$(this).addClass('active')
		.siblings('.active')
		.removeClass('active');
});