$('.form__select').styler();

$('.form__calendar').datepicker({
	setDate: new Date(),
	minDate: new Date(),
	dateFormat: "D d MM",
	beforeShow: function(input, inst)
    {
        inst.dpDiv.css({marginTop: -input.offsetHeight + 'px', marginLeft: input.offsetWidth + 'px'});
    }
});

$('input[type="tel"]').mask('Тел. +7 (999) 999-99-99');