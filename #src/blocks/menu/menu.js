menuInit();
$(window).on('resize', function() {
	menuInit();
});
function menuInit() {
	if ($('.menu').length) {
		if ($(window).width() >= 1200) {
			var menu = $('.menu'),
				list = menu.find('.menu__list'),
				items = menu.find('.menu-item'),
				menuHeight = menu.height();

			if (menu.find('.menu-item:last-child').position().top) {
				$('.menu').find('.menu-more').addClass('active');
			} else {
				$('.menu').find('.menu-more').removeClass('active');
			}

			items.each( function() {
				if ($(this).position().top) {
					$(this).addClass('hide');
				} else {
					$(this).removeClass('hide');
				}
			});
		}
	}
}

$(document).on('click', '.menu-more', function(e) {
	e.preventDefault();

	$('.menu').toggleClass('show-all');
});


$(document).on('click', '.menu-link--drop', function(e) {
	e.preventDefault();

	$(this).closest('.menu-item')
		.toggleClass('active')
		.siblings('.active')
		.removeClass('active');
});

$(document).on('click', '.menu-drop__box-title--drop', function(e) {
	e.preventDefault();

	$(this).closest('.menu-drop__box')
		.toggleClass('active');
});