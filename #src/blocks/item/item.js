$('.item__slider').slick({
	infinite: false,
	slidesToShow: 1,
	slidesToScroll: 1,
	arrows: false,
	dots: true,
	fade: true,
	customPaging: function(slick,index) {
		var targetImage = slick.$slides.eq(index).find('img').attr('src');
		return '<button><img src=" ' + targetImage + ' "/></button>';
	},
	responsive: [
		{
			breakpoint: 991,
			settings: {
				fade: false
			}
		}
	]
});