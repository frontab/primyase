$(document).on('click', '.tabs__btn', function(e) {
	e.preventDefault();

	var attr = $(this).attr('data-box'),
		box = $(this).closest('.tabs')
			.find(`.tabs__box[data-box="${attr}"]`);

		$(this).add(box)
			.addClass('active')
			.siblings('.active')
			.removeClass('active');
});